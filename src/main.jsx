import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider, Navigate } from "react-router-dom";
import LaunchCountdown from "./pages/LaunchCountdown.jsx";

import "./base.module.scss";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Navigate to="/counter-page" />,
  },
  {
    path: "/counter-page",
    element: <LaunchCountdown />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(<RouterProvider router={router} />);
