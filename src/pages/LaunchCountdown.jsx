import Countdown, { zeroPad } from "react-countdown";
import { useRef } from "react";
import { useSearchParams } from "react-router-dom";

import iconFacebook from "../assets/images/icon-facebook.svg";
import iconPinterest from "../assets/images/icon-pinterest.svg";
import iconInstagram from "../assets/images/icon-instagram.svg";

import classes from "./style.module.scss";

const key = {
  title: "WE'RE LAUNCHING SOON",
  day: "DAYS",
  hour: "HOURS",
  minute: "MINUTES",
  second: "SECONDS",
  defaultDate: "2024/07/17",
  iconAlt: "icon-sosmed",
};

const LaunchCountdown = () => {
  const [searchParams] = useSearchParams();
  const date = searchParams.get("date");
  const dateRef = useRef(new Date(date || key.defaultDate));

  const renderDate = (text, title) => (
    <div className={classes.date}>
      <div className={classes.decoration} />

      <div className={classes.dateText}>{text}</div>
      <div className={classes.dateTitle}>{title}</div>
      <div className={classes.decoration2}>
        <div className={classes.circleLeft} />
        <div className={classes.circleRight} />
      </div>
    </div>
  );
  const renderCountDetail = ({ days, hours, minutes, seconds }) => {
    const day = zeroPad(days, 2);
    const hour = zeroPad(hours, 2);
    const minute = zeroPad(minutes, 2);
    const second = zeroPad(seconds, 2);
    return (
      <div className={classes.counter}>
        {renderDate(day, key.day)}
        {renderDate(hour, key.hour)}
        {renderDate(minute, key.minute)}
        {renderDate(second, key.second)}
      </div>
    );
  };

  const renderTitle = () => <div className={classes.title}>{key.title}</div>;

  const renderCountdown = () => (
    <div className={classes.cdWrapper}>
      <Countdown date={dateRef.current} renderer={renderCountDetail} />
    </div>
  );

  const renderMountain = () => (
    <div className={classes.mountain}>
      <img src={iconFacebook} alt={key.iconAlt} className={classes.sosmed} />
      <img src={iconPinterest} alt={key.iconAlt} className={classes.sosmed} />
      <img src={iconInstagram} alt={key.iconAlt} className={classes.sosmed} />
    </div>
  );

  return (
    <div className={classes.wrapper}>
      {renderTitle()}
      {renderCountdown()}
      {renderMountain()}
    </div>
  );
};

export default LaunchCountdown;
